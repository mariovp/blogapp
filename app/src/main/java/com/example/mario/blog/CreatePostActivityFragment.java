package com.example.mario.blog;

import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A placeholder fragment containing a simple view.
 */
public class CreatePostActivityFragment extends Fragment {

    private EditText editTextTitle;
    private EditText editTextAuthor;
    private EditText editTextContent;
    private View view;

    public CreatePostActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_create_post, container, false);

        editTextTitle = (EditText) view.findViewById(R.id.editTextTitle);
        editTextAuthor = (EditText) view.findViewById(R.id.editTextAuthor);
        editTextContent = (EditText) view.findViewById(R.id.editTextContent);

        return view;
    }

    public void publishPost(){

        boolean ready = true;
        String title = editTextTitle.getText().toString();
        String author = editTextAuthor.getText().toString();
        String content = editTextContent.getText().toString();
        if(title.equals("")){
            Snackbar.make(view, "Titulo vacío", Snackbar.LENGTH_LONG).show();
            ready = false;
        }else if(author.equals("")){
            Snackbar.make(view, "Autor vacío", Snackbar.LENGTH_LONG).show();
            ready = false;
        }else if(content.equals("")){
            Snackbar.make(view, "Publicación sin contenido", Snackbar.LENGTH_LONG).show();
            ready = false;
        }

        if (ready) {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = df.format(c.getTime());

            HttpURLConnection urlConnection = null;
            JSONObject jsonObject = new JSONObject();
            try {

                jsonObject.accumulate("title", title);
                jsonObject.accumulate("author", author);
                jsonObject.accumulate("content",content);
                jsonObject.accumulate("postDate",formattedDate);

            } catch (JSONException e) {
                e.printStackTrace();
            }finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }

            if (jsonObject.length() > 0) {
                new SendJsonDataToServer().execute(String.valueOf(jsonObject));
            }
        }

    }

    private class SendJsonDataToServer extends AsyncTask<String,Void,Integer>{

        @Override
        protected Integer doInBackground(String... params) {
            int responseCode=-1;
            String JsonData = params[0];
            HttpURLConnection urlConnection = null;
            try {
                final String POSTS_URL = "http://192.241.230.142/blog/v1/posts";
                URL url = new URL(POSTS_URL);
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setDoOutput(true);

                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept", "application/json");

                Writer writer = new BufferedWriter(new OutputStreamWriter(urlConnection.getOutputStream(), "UTF-8"));
                writer.write(JsonData);

                writer.close();

                responseCode = urlConnection.getResponseCode();
                Log.d("SendJsonDataToServer","Response Code"+responseCode);

            }catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return responseCode;
        }

        @Override
        protected void onPostExecute(Integer code) {
            super.onPostExecute(code);

            if(code == 201){
                Toast.makeText(view.getContext(), "Publicación exitosa", Toast.LENGTH_LONG).show();
                getActivity().finish();
            } else if(code == 409){
                Snackbar.make(view, "Ya existe post con ese titulo", Snackbar.LENGTH_LONG).show();
            }else if(code == 400){
                Snackbar.make(view, "Campos incompletos", Snackbar.LENGTH_LONG).show();
            } else {
                Snackbar.make(view, "Error de la conexión", Snackbar.LENGTH_LONG).show();
            }

        }
    }
}
