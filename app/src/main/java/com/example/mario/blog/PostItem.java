package com.example.mario.blog;

/**
 * Created by mario on 14/06/2016.
 */
public class PostItem implements ListViewItem{

    public final int postId;
    public final String title;
    public final String resName;
    public final String author;
    public final String postDate;
    public final String content;

    public PostItem(int postId, String title, String resName, String author, String postDate, String content) {
        this.postId = postId;
        this.title = title;
        this.resName = resName;
        this.author = author;
        this.postDate = postDate;
        this.content = content;
    }

    @Override
    public boolean isSection() {
        return false;
    }
}
