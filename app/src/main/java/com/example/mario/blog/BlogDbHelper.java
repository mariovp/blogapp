package com.example.mario.blog;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.mario.blog.BlogContract.PostsTable;

/**
 * Created by mario on 15/06/2016.
 */
class BlogDbHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "blog.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + PostsTable.TABLE_NAME + " (" +
                    PostsTable._ID + " INTEGER PRIMARY KEY," +
                    PostsTable.COLUMN_NAME_POSTID + " INTEGER" + COMMA_SEP +
                    PostsTable.COLUMN_NAME_TITLE + TEXT_TYPE + COMMA_SEP +
                    PostsTable.COLUMN_NAME_RESNAME + TEXT_TYPE + COMMA_SEP +
                    PostsTable.COLUMN_NAME_AUTHOR + TEXT_TYPE + COMMA_SEP +
                    PostsTable.COLUMN_NAME_POSTDATE + TEXT_TYPE + COMMA_SEP +
                    PostsTable.COLUMN_NAME_CONTENT + TEXT_TYPE +
            " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + PostsTable.TABLE_NAME;

    public BlogDbHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
