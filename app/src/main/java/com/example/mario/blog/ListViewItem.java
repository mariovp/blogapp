package com.example.mario.blog;

/**
 * Created by mario on 16/06/2016.
 */
interface ListViewItem {

    boolean isSection();

}
