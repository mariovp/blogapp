package com.example.mario.blog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by mario on 14/06/2016.
 */
class PostArrayAdapter extends ArrayAdapter <ListViewItem> {

    private final Context context;
    private final List<ListViewItem> items;
    private final LayoutInflater inflater;
    private final int VIEW_TYPE_POSTITEM = 0;
    private final int VIEW_TYPE_SECTION = 1;

    public PostArrayAdapter(Context context, List<ListViewItem> items) {
        super(context, -1, items);
        this.context = context;
        this.items = items;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ListViewItem item = items.get(position);
        View listItemView;

        if(item.isSection()){
            //Reciclar vistas para mejorar rendimiento
            if(convertView == null){
                listItemView = inflater.inflate(R.layout.list_item_section, parent, false);
            }else{
                if(getItemViewType(position) == VIEW_TYPE_SECTION){
                    listItemView = convertView;
                }else{
                    listItemView = inflater.inflate(R.layout.list_item_section, parent, false);
                }
            }

            TextView section = (TextView) listItemView.findViewById(R.id.sectionTextView);
            SectionItem sectionItem = (SectionItem) item;
            section.setText(sectionItem.title);

        }else{
            if(convertView == null){
                listItemView = inflater.inflate(R.layout.list_item_post, parent, false);
            }else{
                if(getItemViewType(position) == VIEW_TYPE_POSTITEM){
                    listItemView = convertView;
                }else{
                    listItemView = inflater.inflate(R.layout.list_item_post, parent, false);
                }
            }

            TextView title = (TextView) listItemView.findViewById(R.id.list_item_post_title);
            TextView author = (TextView) listItemView.findViewById(R.id.list_item_post_author);
            TextView date = (TextView) listItemView.findViewById(R.id.list_item_post_date);
            TextView content = (TextView) listItemView.findViewById(R.id.list_item_post_content);

            PostItem postItem = (PostItem)item;
            title.setText(postItem.title);
            author.setText(postItem.author);
            date.setText(postItem.postDate);
            if(postItem.content.length()>70){
                content.setText(postItem.content.substring(0,70)+"...");
            }else{
                content.setText(postItem.content);
            }
        }

       return listItemView;
    }

    @Override
    public int getItemViewType(int position) {

        if(items.get(position) instanceof PostItem){
            return VIEW_TYPE_POSTITEM;
        } else {
            return VIEW_TYPE_SECTION;
        }

    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }
}
