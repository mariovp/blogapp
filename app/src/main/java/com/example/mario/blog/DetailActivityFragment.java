package com.example.mario.blog;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailActivityFragment extends Fragment {

    public DetailActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);

        TextView title = (TextView) view.findViewById(R.id.textViewTitle);
        TextView author = (TextView) view.findViewById(R.id.textViewAuthor);
        TextView date = (TextView) view.findViewById(R.id.textViewDate);
        TextView content = (TextView) view.findViewById(R.id.textViewContent);

        Bundle intentData =  getActivity().getIntent().getExtras();

        String titleStr = intentData.getString("title");
        String authorStr = intentData.getString("author");
        String dateStr = intentData.getString("date");
        String contentStr = intentData.getString("content");

        title.setText(titleStr);
        author.setText(authorStr);
        date.setText(dateStr);
        content.setText(contentStr);
        return view;
    }
}
