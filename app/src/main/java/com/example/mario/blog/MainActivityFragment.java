package com.example.mario.blog;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.mario.blog.BlogContract.PostsTable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    private PostArrayAdapter postAdapter;
    private FloatingActionButton addButton;
    private boolean isConnected;
    private PostItem[] postData;
    private List<PostItem> filteredPostData;
    private boolean isFiltered;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View view;
    private ConnectivityManager cm;

    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_main, container, false);

        List<ListViewItem> postList = new ArrayList<>();
        postAdapter = new PostArrayAdapter(getContext(),postList);
        ListView listView = (ListView) view.findViewById(R.id.listView_post);
        listView.setAdapter(postAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListViewItem item = postAdapter.getItem(position);
                if(item instanceof PostItem){
                    PostItem post;
                    if(isFiltered){
                        post = filteredPostData.get(position);
                    }else{
                        post = postData[position];
                    }
                    String title = post.title;
                    String author = post.author;
                    String date = post.postDate;
                    String content = post.content;
                    Intent i = new Intent(getActivity(),DetailActivity.class);
                    i.putExtra("title",title);
                    i.putExtra("author",author);
                    i.putExtra("date",date);
                    i.putExtra("content",content);
                    startActivity(i);
                }
            }
        });

        filteredPostData = new ArrayList<>();

        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new FetchPostsTask().execute();
            }
        });

        //Checar si esta conectado a internet
        cm = (ConnectivityManager)getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        addButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        new FetchPostsTask().execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        new FetchPostsTask();
    }

    public void filterPosts(String query){
        postAdapter.clear();
        filteredPostData.clear();

        List<PostItem> filteredTitle = new ArrayList<>();
        List<PostItem> filteredAuthor = new ArrayList<>();
        List<PostItem> filteredContent = new ArrayList<>();

        //Filtro por titulo
        for (PostItem aPostData : postData) {
            if (aPostData.title.toLowerCase().contains(query)) {
                filteredTitle.add(aPostData);
            }
        }
        if(filteredTitle.size()>0){
            SectionItem sectionTitle = new SectionItem("Titulo");
            postAdapter.add(sectionTitle);
            postAdapter.addAll(filteredTitle);
            filteredPostData.add(null);
            filteredPostData.addAll(filteredTitle);
        }

        //Filtro por autor
        for (PostItem aPostData : postData) {
            if (aPostData.author.toLowerCase().contains(query)) {
                filteredAuthor.add(aPostData);
            }
        }
        if(filteredAuthor.size()>0){
            SectionItem sectionTitle = new SectionItem("Autor");
            postAdapter.add(sectionTitle);
            postAdapter.addAll(filteredAuthor);
            filteredPostData.add(null);
            filteredPostData.addAll(filteredAuthor);
        }

        //Filtro por contenido
        for (PostItem aPostData : postData) {
            if (aPostData.content.toLowerCase().contains(query)) {
                filteredContent.add(aPostData);
            }
        }
        if(filteredContent.size()>0){
            SectionItem sectionTitle = new SectionItem("Contenido");
            postAdapter.add(sectionTitle);
            postAdapter.addAll(filteredContent);
            filteredPostData.add(null);
            filteredPostData.addAll(filteredContent);
        }

        isFiltered = true;

    }

    public void unfilterPosts(){
        postAdapter.clear();
        postAdapter.addAll(postData);

        isFiltered = false;
    }

    public class FetchPostsTask extends AsyncTask<Void, Void, PostItem[]>{
        private final String LOG_TAG = FetchPostsTask.class.getSimpleName();

        @Override
        protected void onPreExecute() {

            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            isConnected = activeNetwork != null &&
                    activeNetwork.isConnectedOrConnecting();

            if(isConnected){
                if (addButton.getVisibility() == View.INVISIBLE){
                    Snackbar.make(view, "Conexión establecida", Snackbar.LENGTH_SHORT).show();
                }
                addButton.setVisibility(View.VISIBLE);
            } else{
                addButton.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        protected PostItem[] doInBackground(Void... params) {

            if(isConnected){
                return getBlogDataFromServer();
            } else {
                Snackbar.make(view, "Sin Internet, se muestran datos en caché", Snackbar.LENGTH_INDEFINITE).show();
                return getFromLocalBd();
            }

        }

        @Override
        protected void onPostExecute(PostItem[] result) {


            postData = result;
            if(result!=null){
                postAdapter.clear();
                for (PostItem item:result){
                    postAdapter.add(item);
                }
            }else{
                Snackbar.make(view, "Blog vacío", Snackbar.LENGTH_INDEFINITE).show();
            }

            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(false);
                }
            });

        }

        @Nullable
        private PostItem[] getBlogDataFromServer() {
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            String postsJsonStr = null;

            try {
                final String POSTS_URL = "http://192.241.230.142/blog/v1/posts";
                URL url = new URL(POSTS_URL);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuilder buffer = new StringBuilder();
                if (inputStream == null) {
                    // Nothing to do.
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line).append("\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    return null;
                }
                postsJsonStr = buffer.toString();
                Log.v(LOG_TAG,"Posts JSON string: "+postsJsonStr);

            } catch (IOException e) {
                Log.e(LOG_TAG, e.toString());
                return null;
            }finally{
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e("MainActivityFragment", "Error closing stream", e);
                    }
                }
            }

            try {
                PostItem[] posts = getPostsDataFromJson(postsJsonStr);
                saveInLocalBd(posts);
                return posts;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
        }



        private PostItem[] getPostsDataFromJson(String postsJsonStr) throws JSONException{

            final String API_POSTID = "postID";
            final String API_TITLE = "title";
            final String API_RESNAME = "resName";
            final String API_AUTHOR = "author";
            final String API_DATE = "postDate";
            final String API_CONTENT = "content";

            JSONObject postsJson = new JSONObject(postsJsonStr);
            JSONArray postsArray = postsJson.getJSONArray("post");
            int postsNum = postsArray.length();

            PostItem[] items = new PostItem[postsNum];
            for(int i=0;i<postsNum;i++){

                JSONObject post = postsArray.getJSONObject(i);
                int postId = post.getInt(API_POSTID);
                String title = post.getString(API_TITLE);
                String resName = post.getString(API_RESNAME);
                String author = post.getString(API_AUTHOR);
                String postDate = post.getString(API_DATE);
                String content = post.getString(API_CONTENT);

                PostItem postItem = new PostItem(postId, title, resName, author, postDate, content);

                items[i] = postItem;

            }

            return items;
        }

        public void saveInLocalBd(PostItem[] posts){
            BlogDbHelper mDbHelper = new BlogDbHelper(getContext());

            SQLiteDatabase db = mDbHelper.getWritableDatabase();

            db.delete("post",null,null);

            for (PostItem post : posts) {
                ContentValues values = new ContentValues();
                values.put(PostsTable.COLUMN_NAME_POSTID, post.postId);
                values.put(PostsTable.COLUMN_NAME_TITLE, post.title);
                values.put(PostsTable.COLUMN_NAME_RESNAME, post.resName);
                values.put(PostsTable.COLUMN_NAME_AUTHOR, post.author);
                values.put(PostsTable.COLUMN_NAME_POSTDATE, post.postDate);
                values.put(PostsTable.COLUMN_NAME_CONTENT, post.content);
                db.insert(PostsTable.TABLE_NAME, null, values);
            }
            Log.v(LOG_TAG,"Guardado en SQLite exitosamente");

            db.close();

        }

        public PostItem[] getFromLocalBd(){
            BlogDbHelper mDbHelper = new BlogDbHelper(getContext());

            SQLiteDatabase db = mDbHelper.getReadableDatabase();

            Cursor c = db.rawQuery("SELECT * FROM post",null);
            int postsNum = c.getCount();
            PostItem [] posts = new PostItem[postsNum];

            for(int i=0; i<postsNum; i++){
                c.moveToPosition(i);
                int postID = c.getInt(c.getColumnIndex("postID"));
                String title = c.getString(c.getColumnIndex("title"));
                String resName = c.getString(c.getColumnIndex("resName"));
                String author = c.getString(c.getColumnIndex("author"));
                String postDate = c.getString(c.getColumnIndex("postDate"));
                String content = c.getString(c.getColumnIndex("content"));

                posts[i] = new PostItem(postID, title, resName, author, postDate, content);
            }
            Log.v(LOG_TAG,"Leido de SQLite exitosamente");

            c.close();
            db.close();

            return posts;
        }
    }


}
