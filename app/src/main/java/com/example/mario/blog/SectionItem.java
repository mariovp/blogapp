package com.example.mario.blog;

/**
 * Created by mario on 16/06/2016.
 */
public class SectionItem implements ListViewItem {

    public final String title;

    public SectionItem(String title) {
        this.title = title;
    }

    @Override
    public boolean isSection() {
        return true;
    }
}
