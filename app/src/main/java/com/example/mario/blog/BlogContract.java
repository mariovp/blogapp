package com.example.mario.blog;

import android.provider.BaseColumns;

/**
 * Created by mario on 15/06/2016.
 */
final class BlogContract {

    public BlogContract(){}

    public static abstract class PostsTable implements BaseColumns{
        public static final String TABLE_NAME = "post";
        public static final String COLUMN_NAME_POSTID = "postID";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_RESNAME = "resName";
        public static final String COLUMN_NAME_AUTHOR = "author";
        public static final String COLUMN_NAME_POSTDATE = "postDate";
        public static final String COLUMN_NAME_CONTENT = "content";
    }

}
